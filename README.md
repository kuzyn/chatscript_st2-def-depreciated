# DEPRECIATED #

# NOW AT #
# https://github.com/joshuasnowball/chatscript-tmlanguage #

# README #

### Summary ###

A ChatScript syntax file to use with Sublime Text 2.

Version 0.3
May 2014

### Usage ###

* Put the tmLanguage file in your Package/User folder (Windows: %APPDATA%\Sublime Text 2 | OSX: ~/Library/Application Support/Sublime Text 2 | Linux: ~/.config/sublime-text-2)
* Restart Sublime Text 2
* You can now select View/Syntax/Chatscript from the menu 
* You can edit and compile/convert the JSON source by using AAAPackageDev
* Should work with any IDE that support tmLanguage files
* Matching rely on Oniguruma Regex rules.

### Refs ###
* http://docs.sublimetext.info/en/latest/extensibility/syntaxdefs.html#your-first-syntax-definition
* http://manual.macromates.com/en/language_grammars#naming_conventions.html
* http://www.geocities.jp/kosako3/oniguruma/doc/RE.txt
* http://chatscript.sourceforge.net/Documentation/
* http://tmtheme-editor.herokuapp.com/#/theme/Monokai